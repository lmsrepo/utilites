from sparkpost import SparkPost
from sparkpost.exceptions import SparkPostAPIException
from pandas import DataFrame
import os
import sys
import csv

# This script sends emails through Sparkpost. It reads in a CSV and reports out with some printed statements for status
# Most status is maintained in the CSV files that are output.
# This script sends to each email address with a separate transmissions. This can be slower but can catch errors
# for each individual email adress


# variables to change for running the script
# file names will automatically have '.csv' added to them
# The emailListCSV should have only one email per line with NO header
emailListCSV = '/Users/ssmith/Documents/EmailListener/LegalTermsUpdate/emailLists/emailList'
successCSV = '/Users/ssmith/Documents/EmailListener/LegalTermsUpdate/scriptResults/successList-final'
failureCSV = '/Users/ssmith/Documents/EmailListener/LegalTermsUpdate/scriptResults/failureList-final'
emailCampaign = 'OCEAN-TermsUpdate-2020Mar'
emailTemplate = 'ocean-legal-terms-update'
reportingInterval = 200
listNumber = 2


# Sparkpost API key must be configured as the 'SPARKPOST_API_KEY' environment variable
# This constructor will use the value of that variable so the API key is not in the code
sp = SparkPost()

# Dictionaries for storing results for export to CSV files later
successList = {}
failureList = {}
successList['email'] = ['email']
successList['transmissionId'] = ['transmissionId']
failureList['email'] = ['email']
failureList['error'] = ['error']
count = 0

emailList = []

emailListCSVFile = emailListCSV + str(listNumber) + '.csv'
successCSVFile = successCSV + str(listNumber) + '.csv'
failureCSVFile = failureCSV + str(listNumber) + '.csv'

if os.path.exists(emailListCSVFile):
    with open(emailListCSVFile,'r') as l:
        try:
            reader = csv.reader(l)
            for row in reader:
                emailList.append(row[0])
            l.close()
        except:
            print("Error opening email list file")
else:
    print("The Email List input file does not exist!")
    sys.exit()
if os.path.exists(successCSVFile):
    print("Emails have already been sent for this list!")
    sys.exit()


for email in emailList:
    try:
        count += 1
        if count % reportingInterval == 0:
            print("Working on email #" + str(count))
        response = sp.transmissions.send(
            recipients = [email],
            campaign = emailCampaign,
            template = emailTemplate
        )
        successList['email'].append(email)
        successList['transmissionId'].append(response['id'])
    except SparkPostAPIException as err:
        failureList['error'].append(err.errors)
        failureList['email'].append(email)

print("Finished sending emails. ")
print("Number sent successfully: " + str(len(successList['email']) - 1))
print("Number of errors: " + str(len(failureList['email']) - 1))

DataFrame.from_dict(successList, orient='columns').to_csv(successCSVFile, mode='w', na_rep='NULL',
                                                          index=False, header=False)
DataFrame.from_dict(failureList, orient='columns').to_csv(failureCSVFile, mode='w', na_rep='NULL',
                                                          index=False, header=False)
