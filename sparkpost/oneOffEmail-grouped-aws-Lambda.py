from sparkpost import SparkPost
from sparkpost.exceptions import SparkPostAPIException
import os
import sys
import csv

# This script sends emails through Sparkpost. It reads in a CSV and reports out with some printed statements for status
# Most status is maintained in the CSV files that are output.
# This script sends to groups of email addresses with each transmissions. This is faster but does not capture the exact
# error for any individual email address. The overall transmission can be a "success" but some of the email addresses
# may be rejected by Sparkpost. A message is output about there being some rejections but no other details.
# Retrying rejected emails should result in the same response so it is not unreasonable that these are "successes"


# variables to change for running the script
# file names will automatically have '.csv' added to them
# The emailListCSV should have only one email per line with NO header
script_dir = os.path.dirname(__file__)
emailListCSV = os.path.join(script_dir, 'MobileTerms/emailLists/emailList')
successCSV = os.path.join(script_dir, 'MobileTerms/scriptResults/successList-final')
failureCSV = os.path.join(script_dir, 'MobileTerms/scriptResults/failureList-final')
emailCampaign = 'ocean-mobile-terms-update'
emailTemplate = 'ocean-mobile-terms-update'
groupSize = 20
listNumber = 1


# Sparkpost API key must be configured as the 'SPARKPOST_API_KEY' environment variable
# This constructor will use the value of that variable so the API key is not in the code
sp = SparkPost('SPARKPOST_API_KEY')

# Dictionaries for storing results for export to CSV files later
successList = {}
failureList = {}
successList['email'] = ['email']
successList['transmissionId'] = ['transmissionId']
failureList['email'] = ['email']
failureList['error'] = ['error']




def start(input, context):
    emailList = []
    count = 0
    emailListCSVFile = emailListCSV + str(listNumber) + '.csv'
    successCSVFile = successCSV + str(listNumber) + '.csv'
    failureCSVFile = failureCSV + str(listNumber) + '.csv'

    if os.path.exists(emailListCSVFile):
        with open(emailListCSVFile,'r') as l:
            try:
                reader = csv.reader(l)
                for row in reader:
                    emailList.append(row[0])
                l.close()
            except:
                print("Error opening email list file")
    else:
        print("The Email List input file does not exist!")
        sys.exit()
    if os.path.exists(successCSVFile):
        print("Emails have already been sent for this list!")
        sys.exit()

    # Start with empty recipient list
    recipientList = []
    for email in emailList:
        count += 1
        # append to recipient list
        recipientList.append(email)
        if count % groupSize == 0:
            print("Working on email #" + str(count))
            # send group
            sendEmail(recipientList)
            # empty recipient list
            recipientList = []
    # send last group
    sendEmail(recipientList)

    print("Finished sending emails. ")
    print("Number sent successfully: " + str(len(successList['email']) - 1))
    print("Number of errors: " + str(len(failureList['email']) - 1))

    # print(*failureList['email'], sep = "\n")

def sendEmail(sendingList):
    try:
        response = sp.transmissions.send(
            recipients = sendingList,
            campaign = emailCampaign,
            template = emailTemplate
        )
        # track transmission numbers
        num_rejected = response['total_rejected_recipients']
        if num_rejected > 0:
            print("Response had " + str(num_rejected) + " rejected recipients")
        # process success list
        for toAdd in sendingList:
            successList['email'].append(toAdd)
            successList['transmissionId'].append(response['id'])
    except SparkPostAPIException as err:
        #process failure list
        for toAdd in sendingList:
            failureList['error'].append(err.errors)
            failureList['email'].append(toAdd)

