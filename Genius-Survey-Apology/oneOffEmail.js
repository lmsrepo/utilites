const fetch = require("node-fetch");
const csv = require("csv-parser");
const fs = require("fs");
const awsParamStore = require("aws-param-store");

const hologic = awsParamStore.getParameterSync('/hologic/dev/sparkpost', { region: 'us-east-1' }).Value;
const d = new Date();
const current = d.toISOString().split(':').splice(0,2).join(':');

let recipientsArray = [];
let opensArray = [];
let injectionsArray = [];
let filteredArray = [];

async function populateArray(arrName, arr, requestUrl) {
  const baseUrl = "https://api.sparkpost.com";
  let response = await fetch(`${baseUrl}${requestUrl}`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      Authorization: hologic,
    }
  });

  let parsedResponse = await response.json();

  // push all rcpt_to emails to array that was passed in
  parsedResponse.results.forEach(result => {
    if (!arr.includes(result.rcpt_to.toLowerCase())) {
      arr.push(result.rcpt_to.toLowerCase());
    }
  });

  console.log(`${arrName} length`);
  console.log(arr.length);

  // call function recursively until there are no pages left
  if (parsedResponse.links && parsedResponse.links.next) {
    await populateArray(arrName, arr, parsedResponse.links.next);
  }
}

async function filterDuplicates(outerArr, innerArr) {
  filteredArray = outerArr.filter(open => {
    if (innerArr.includes(open)) {
      return false;
    }
    return true;
  });
  console.log('filteredArray.length');
  console.log(filteredArray.length);
}

async function loadRecipients() {
  // put csv file name here
  fs
  .createReadStream("apologyEmailRecipients.csv")
  .pipe(csv())
  .on("data", (row) => {
    // check every email against another array of objects containing email and uuids and push if there is a match
    if (filteredArray.includes(row.email.toLowerCase())) {
      recipientsArray.push({
        address: {
          email: row.email.toLowerCase(),
        },
        substitution_data: {
          uuid: row.uuid,
        },
      });
    }
  })
  .on("end", () => {
    console.log("recipients loaded");
    console.log('recipients length');
    console.log(recipientsArray.length);
    sendEmails();
  });
};

async function sendEmails() {
  console.log('emails sent here');
  // let response = await fetch(
  //   "https://api.sparkpost.com/api/v1/transmissions?num_rcpt_errors=3",
  //   {
  //     method: "POST",
  //     headers: {
  //       Accept: "application/json",
  //       Authorization: hologic,
  //     },
  //     body: JSON.stringify({
  //       content: {
  //         template_id: "genius-survey-apology"
  //       },
  //       recipients: recipientsArray,
  //     }),
  //   }
  // );
  // console.log(await response.json());
};

async function app() {
  await populateArray("opensArray", opensArray, `/api/v1/events/message?events=initial_open&from=2021-04-20T15:00&page=5&per_page=10000&subaccounts=1&templates=genius-survey-follow-up&to=${current}`);
  await populateArray("injectionsArray", injectionsArray, `/api/v1/events/message?cursor=initial&events=injection,bounce&from=2021-04-20T15:00&page=5&per_page=10000&subaccounts=1&templates=genius-survey-apology&to=${current}`);
  await filterDuplicates(opensArray, injectionsArray);
  await loadRecipients();
}

console.log(`running at ${current}`);
app();