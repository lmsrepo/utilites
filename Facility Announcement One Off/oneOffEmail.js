// one off email sender with uuid

//questions for later - is phone number needed? check template

const fetch = require("node-fetch");
const csv = require("csv-parser");
const fs = require("fs");
const awsParamStore = require("aws-param-store");

let hologic;
let recipientsArray = [];
let facilitiesArray = [];
const populateFacilitiesArray = async () => {
  fs.createReadStream("facilities.csv")
    .pipe(csv())
    .on("data", (row) => {
      facilitiesArray.push({
        search_zipcode: row.search_zipcode,
        fname1: row.fname1,
        street1: row.street1,
        city1: row.city1,
        state1: row.state1,
        zip1: row.zip1,
        phone1: row.phone1,
        fname2: row.fname2,
        street2: row.street2,
        city2: row.city2,
        state2: row.state2,
        zip2: row.zip2,
        phone2: row.phone2,
        fname3: row.fname3,
        street3: row.street3,
        city3: row.city3,
        state3: row.state3,
        zip3: row.zip3,
        phone3: row.phone3,
      });
    })
    .on("end", () => {
      console.log("facility CSV file successfully processed");
      prep();
    });
};

const prep = async () => {
  await awsParamStore
    .getParameter("/hologic/dev/sparkpost", { region: "us-east-1" })
    .then((parameter) => {
      hologic = parameter.Value;
    });

  // put csv file name here
  // csv with emails and zips will go here
  fs.createReadStream("testEmails.csv")
    .pipe(csv())
    .on("data", (row) => {
      // Store index of facilitiesArray where search_zipcode matches recipient's zipcode
      // (if user’s zip is 98004, find index where facilitiesArray[x].search_zipcode = 98004)

      //   loop through facilitiesArray push facility information to recipientsArray if zipcodes match
      for (let i = 0; i < facilitiesArray.length; i++) {
        if (facilitiesArray[i].search_zipcode == row.zipcode) {
          recipientsArray.push({
            address: {
              email: row.email,
            },
            substitution_data: {
              fname1: facilitiesArray[i].fname1,
              street1: facilitiesArray[i].street1,
              city1: facilitiesArray[i].city1,
              state1: facilitiesArray[i].state1,
              zip1: facilitiesArray[i].zip1,
              phone1: facilitiesArray[i].phone1,
              fname2: facilitiesArray[i].fname2,
              street2: facilitiesArray[i].street2,
              city2: facilitiesArray[i].city2,
              state2: facilitiesArray[i].state2,
              zip2: facilitiesArray[i].zip2,
              phone2: facilitiesArray[i].phone2,
              fname3: facilitiesArray[i].fname3,
              street3: facilitiesArray[i].street3,
              city3: facilitiesArray[i].city3,
              state3: facilitiesArray[i].state3,
              zip3: facilitiesArray[i].zip3,
              phone3: facilitiesArray[i].phone3,
            },
          });
          return;
        }
      }
    })
    .on("end", () => {
      console.log("CSV file successfully processed");
      // sendEmail();
    });
};
const sendEmail = async () => {
  let response = await fetch(
    "https://api.sparkpost.com/api/v1/transmissions?num_rcpt_errors=3",
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        Authorization: hologic,
      },
      body: JSON.stringify({
        //content needs to be changed to match template-id
        content: {
          template_id: "new-site-seattle",
        },
        recipients: recipientsArray,
      }),
    }
  );
  console.log(await response.json());
};

populateFacilitiesArray();