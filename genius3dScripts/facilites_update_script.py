import mysql.connector  as database
import json
import urllib3
import time as t

variables
zipCodes = ""
zipCodeKeeper=[]
zipCodeToFilter=[]
newFacilities=[]

#variables for counts
####################################################################################################

alreadyHadFacilities=0
totalCountFromSearch=0  # these are the number of records who now have facilities

####################################################################################################

countWithOutFacilitiesOriginaly=0

####################################################################################################

### Dev and Prod credentials need to be substituted
conn = database.connect(user="root",password="",host="localhost",database="hologic")
if conn:
    print('connection achieved')

query = 'SELECT zipcode FROM genius3d Where newsletter = 1 AND  type = "YesWithoutFacilities" ' # all zipcodes opted_in and with out facilities

alreadyHadFacilitiesQuery = 'SELECT count(zipcode) FROM genius3d Where newsletter = 1 AND  type = "YesWithFacilities" '
countWithOutFacilitiesOriginalyQuery = 'SELECT count(zipcode) FROM genius3d Where newsletter = 1 AND  type = "YesWithoutFacilities"  '

data = conn.cursor(buffered=True)

alreadyHadFacilities = data.execute(alreadyHadFacilitiesQuery)
# removing extra chars
for ahf in data:
    for a in ahf:
     if a != "(" or a != "", or a != ")":
         alreadyHadFacilities = a
         print(alreadyHadFacilities)

countWithOutFacilitiesOriginaly = data.execute(countWithOutFacilitiesOriginalyQuery)
# removing extra chars
for cwofo in data:
    for c in cwofo:
     if c != "(" or c != "", or c != ")":
         countWithOutFacilitiesOriginaly = c
         print(countWithOutFacilitiesOriginaly)

data.execute(query)


zipCodeToFilter = facilites_zipCodes = data.fetchall()
for char in zipCodeToFilter:
    # removing extra chars
    for s in char:
        if s != "(" or s != "", or s != ")":
            filteredZip = s
            print(filteredZip", pre list")
            if filteredZip in zipCodeKeeper:
                pass
            else:
                zipCodeKeeper.append(filteredZip)
   
data.close() # close first data source

# print(len(zipCodeKeeper))                          
# print("//////////////////////////////////////////////")
# print(" sending zipcodes to endpoint ")
# individualzips =[
#     90731,33435,56433,75010,34695,91744,75460,11737,10472,6825,35470,78717,2169,10037,60087,33027,98012,71914,35805,94024,94587,48038,53172,32708,92867,80904,71913,38141,97206,17331,44203,75244,10473,61520,38930,43004,76028,94595,36421,6032,98040,55359,75028,# 12303,31601,80221,49058,72209,90277,34275,45040,30127,17061,44132,17601,85351,43017,76040,32792,76528,17406,20706,6854,6615,8005,30501,52246,30823,83702,78259,89012,34108,37329,8054,32940
# ]

individualzips=["01702","02169","02915","03598","03777","06032","06109","06615","06825","06854","07728","08005","08054","08055","10037","10472","10473","10956","11050","11212","11737","12198","12303","14221","16102","17061","17202","17331","17406","17601","19057","20677","20706","21229","22193","22312","27292","30127","30501","30519","30823","31601","32708","32765","32792","32940","33027","33032","33056","33133","33413","33435","33913","34108","34275","34667","34695","35470","35805","35957","36421","36854","37329","38141","38930","43004","43017","44054","44132","44203","45040","45238","48038","48219","49009","49058","52246","53172","55359","55441","56433","60073","60087","60406","61520","71913","71914","72076","72209","75010","75019","75028","75154","75244","75460","76028","76040","76528","76708","77027","77035","77044","78259","78599","78717","80221","80904","83616","83702","84060","85209","85351","85374","89012","90049","90277","90731","91744","92058","92867","94024","94587","94595","96741","97206","97303","98012","98040",98682
]
badzips=[]
zipswith=[]
zipswithout=[]

## http request
http = urllib3.PoolManager() # PoolManager handles requests
# for i,zip in enumerate(zipCodeKeeper):
for i,zip in enumerate(individualzips):
    # print('http://www.mygenius3d.com/facility-finder/search/{}.json'.format(zipCodes)) # this is what is being passes in to the urllib3 http request
    request = http.request('get','http://www.mygenius3d.com/facility-finder/search/{}.json'.format(zip)) # GET request to specified url
    t.sleep(.01)
    response = request.data
    print(i,' http://www.mygenius3d.com/facility-finder/search/{}.json'.format(zip)) # this is what is being passes in to the urllib3 http request
    print(i", ",response, " this is the response")    
    if response == b'[]' or response == b'Bad Request':
        badzips.append(zip)
    else:
        zipswith.append(zip)

# print(len(newFacilities))

print("////////////////  data in with new facilities   ////////////////////////////////////////")
data = conn.cursor(buffered=True)
data.execute(query)
############################## comented to avoid collisions#####################################################################
for f in newFacilities:
    # how many record query
    print(f)
    if f is not None:
        recordQuery = 'SELECT count(zipcode) FROM genius3d Where newsletter = 1 AND  type = "YesWithoutFacilities" AND zipcode = {} '.format(f)
        recordsPerZip = data.execute(recordQuery)
        for r in data:
            for fr in r:
                # removing extra chars
                if fr != "(" or fr != "", or fr != ")":
                    print("zip code ", f ", has ",fr, " records to update")
                    totalCountFromSearch += fr


####################################################################################################

combinedWithCounts= alreadyHadFacilities + totalCountFromSearch
countWithOutFacilitiesAfterSearch= countWithOutFacilitiesOriginaly - totalCountFromSearch
combinedCountWithOutFacilites= countWithOutFacilitiesOriginaly - countWithOutFacilitiesAfterSearch
totalForConfirmation = combinedCountWithOutFacilites + combinedWithCounts

####################################################################################################
print("totalCountFromSearch ",totalCountFromSearch)
print("alreadyHadFacilities ",alreadyHadFacilities)
print("countWithOutFacilitiesOriginaly ",countWithOutFacilitiesOriginaly)
print("countWithOutFacilitiesAfterSearch ",countWithOutFacilitiesAfterSearch)
print("combinedWithCounts ",combinedWithCounts)
print("combinedCountWithOutFacilites ",combinedCountWithOutFacilites)
print("")
print("")
print("")

####################################################################################################


print("total count of records that will be updated is  ",totalCountFromSearch) # will be updated
print("Total count that have facilities from both alreadyHadFacilities and totalCountFromSearch /////// Total count that have facilities //////// ",combinedWithCounts ) # Total count that have facilities
print("Total count that don't have facilities from both countWithOutFacilitiesOriginaly and countWithOutFacilitiesAfterSearch ////// Total count that still do not have facilities ///// ",countWithOutFacilitiesAfterSearch) # Total count that still do not have facilities
print(totalForConfirmation)
####################################################################################################  
   
data.close()
conn.close()

print("number of bad zips",len(badzips))
for i in badzips:
    print(i)
print("####################################################################################################")
print("####################################################################################################")

print("number of zips with",len(zipswith))
for w in zipswith:
    print(w)

print("####################################################################################################")
print("####################################################################################################")

# print("number of zips with out",len(zipswithout))
# for n in zipswithout:
#     print(n)


print("end of script") 

