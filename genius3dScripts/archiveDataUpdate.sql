insert into hologic.genius3d_facilities (id,fname1,street1,city1,state1,zip1,phone1,fname2,street2,city2,state2,zip2,phone2,fname3,street3,city3,state3,zip3,phone3)
	select CONCAT('archive-YWF-',CAST(id as CHAR(36))) as id,Facility_Name1,Street1,City1,State1,Zip1,Phone1,Facility_Name2,Street2,City2,State2,Zip2,phone2,Facility_Name3,Street3,City3,State3,Zip3,Phone3
	from hologic_cms.Genius3D_Yes_Facilities
	where Unsub = 0;

insert into hologic.genius3d (id,email_address,newsletter,type,genius3d_facilities_id)
	select CONCAT('archive-YWF-',CAST(id as CHAR(36))) as id,Email,Newsletter,"YesWithFacilities",CONCAT('archive-YWF-',CAST(id as CHAR(36))) as genius3d_facilities_id
	from hologic_cms.Genius3D_Yes_Facilities
	where Unsub = 0;

insert into hologic.unsubscribed_genius3d (email,opt_in,type) 
	select Email,Newsletter,"YesWithFacilities"
		from hologic_cms.Genius3D_Yes_Facilities
	where Unsub = 1;



insert into hologic.genius3d_facilities (id,fname1,street1,city1,state1,zip1,phone1,fname2,street2,city2,state2,zip2,phone2,fname3,street3,city3,state3,zip3,phone3)
    select CONCAT('archive-SPYF-',CAST(id as CHAR(36))) as id,Facility_Name1,Street1,City1,State1,Zip1,Phone1,Facility_Name2,Street2,City2,State2,Zip2,phone2,Facility_Name3,Street3,City3,State3,Zip3,Phone3
	from hologic_cms.Genius_3D_Share_Poll_5_23_thru_9_12_Yes_Facilities
	where Unsub = 0;

insert into hologic.genius3d (id,email_address,newsletter,type,genius3d_facilities_id)
	select CONCAT('archive-SPYF-',CAST(id as CHAR(36))) as id,Email_Address,Newsletter,"YesWithFacilities",CONCAT('archive-SPYF-',CAST(id as CHAR(36))) as genius3d_facilities_id
	from hologic_cms.Genius_3D_Share_Poll_5_23_thru_9_12_Yes_Facilities
	where Unsub = 0;

insert into hologic.unsubscribed_genius3d (email,opt_in,type) 
	select Email_Address,Newsletter,"YesWithFacilities"
		from hologic_cms.Genius_3D_Share_Poll_5_23_thru_9_12_Yes_Facilities
	where Unsub = 1;



insert into hologic.genius3d (id,email_address,newsletter,type)
	select CONCAT('archive-SPNF-',CAST(id as CHAR(36))) as id,Email_Address,Newsletter,"YesWithOutFacilities"
	from hologic_cms.Genius_3D_Share_Poll_5_23_thru_9_12_No_Facilities
	where Unsub = 0;


insert into hologic.genius3d (id,email_address,newsletter,type)
	select CONCAT('archive-YNF-',CAST(id as CHAR(36))) as id,Email,Newsletter,"YesWithOutFacilities"
	from hologic_cms.Genius3D_No_Facilities;




-- this last query combines the 2 tables NO_Facilitites and Yes_Facilities getting all-- 


insert into hologic.unsubscribed_genius3d(email,opt_in,type,date_added)
select distinct Genius3D_Yes_Facilities.Email,Genius3D_Yes_Facilities.Newsletter,"YesWithOutFacilities",current_date() 
from hologic_cms.Genius3D_No_Facilities right  join hologic_cms.Genius3D_Yes_Facilities on Genius3D_No_Facilities.Email = Genius3D_Yes_Facilities.Email  
WHERE NOT EXISTS(SELECT * FROM hologic.unsubscribed_genius3d WHERE hologic.unsubscribed_genius3d.email = Genius3D_Yes_Facilities.Email);
   

