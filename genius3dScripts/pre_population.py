import os
import requests
import json
from pandas import DataFrame
import datetime
import csv
import time as t 

# array_list_file="" 
array_list_file = "./scriptResults/UniqueFindMyGeniusZipcodes.csv"
#  path/name here ^^^^^^^^
facilities={}
array_list_from_file=[] 
array_list=''
no_facilities=[]
count=0
#  manual list of zipcodes
# array_list_manual = ["zipcodes","34231","42001","30064","78119","10950","72212","48891","49097","78957","7869","11710","48430","86301","17701","60462","45365","54868","94803","33025","43560","42328","14001","1756","34293","60491","75125","23606","31522","23229","90740","85901","37664","33955","15108","1854","72529","98682","28031","49404","93950","34236","47532","11412","7901","48650","12528","60634","10466","93428","98540","16150","28226","18403","38016","37760","32404","15914","12203","27410","77262","44515","37335","32776","94561","37027","7042","76627","92020","44107","29575","17353","53158","48624","59047","10475","11249","95363","71112","79504","6117","21770","8844","95476","5641"]   

# sample array for testing. 78119 has no results, 86301 has only 1 facility, 2118 tests adding a zero for five digit zip
array_list_manual=["34231","42001","30064","78119","10950","86301","2118"]

# read csv file / use small array for testing
if os.path.exists(array_list_file):
    with open(array_list_file,'r') as l:
        try:
            reader = csv.reader(l)   
            for row in reader:
                array_list_from_file.append(row[0])
            l.close()
        except:
            print("error")
            # pass

 
if len(array_list_from_file) > 0:
    array_list = array_list_from_file
    # print(array_list, 'if block')
else:
    array_list = array_list_manual

print(array_list)
for zip in array_list:
    count += 1
    if count % 100 == 0:
        print("Working on facility #" + str(count))
    #  accounting for 4 digit numbers by appending the string "0"
    if len(zip) < 4 :
        zip = "00" + zip     
        # print(zip)‰
    elif len(zip) < 5:
        zip = "0" + zip

  
    response = requests.get('https://www.mygenius3d.com/wp-content/plugins/hol-find-location/json-search.php?zip={}'.format(zip))
    # print(zip)
    # print(response.text," text")
    if response.text != '' and response.text != '[]': 
        facilities[zip] = (json.loads(response.text))
    else:
        print(zip +" didnt work")
        no_facilities.append(zip)
        continue

    t.sleep(.01)
    # print(facilities, "facility")

print(str(count) + " zips queried")
print(str(len(facilities)) + " returned results")

#  pull the data we want and add it to the data dictionary

data = {}

data['search_zipcode'] = ['search_zipcode']
data['date_updated'] = ['date_updated']
data['fname1'] = ['fname1']
data['street1'] = ['street1']
data['city1'] = ['city1']
data['state1'] = ['state1']
data['zip1'] = ['zip1']
data['phone1'] = ['phone1']
data['fname2'] = ['fname2']
data['street2'] = ['street2']
data['city2'] = ['city2']
data['state2'] = ['state2']
data['zip2'] = ['zip2']
data['phone2'] = ['phone2']
data['fname3'] = ['fname3']
data['street3'] = ['street3']
data['city3'] = ['city3']
data['state3'] = ['state3']
data['zip3'] = ['zip3']
data['phone3'] = ['phone3']

for k in facilities:
    data['search_zipcode'].append(k)
    data['date_updated'].append(str(datetime.date.today()))
    found = facilities[k]
    # print(found)
    if len(found) >= 1:
        data['fname1'].append(found[0]['practice'])
        data['street1'].append(found[0]['address1'])
        data['city1'].append(found[0]['city'])
        data['state1'].append(found[0]['state'])
        data['zip1'].append(found[0]['zip'])
        data['phone1'].append(found[0]['phone'])
    else:
        data['fname1'].append(None)
        data['street1'].append(None)
        data['city1'].append(None)
        data['state1'].append(None)
        data['zip1'].append(None)
        data['phone1'].append(None)
    if len(found) >= 2:
        data['fname2'].append(found[1]['practice'])
        data['street2'].append(found[1]['address1'])
        data['city2'].append(found[1]['city'])
        data['state2'].append(found[1]['state'])
        data['zip2'].append(found[1]['zip'])
        data['phone2'].append(found[1]['phone'])
    else:
        data['fname2'].append(None)
        data['street2'].append(None)
        data['city2'].append(None)
        data['state2'].append(None)
        data['zip2'].append(None)
        data['phone2'].append(None)
    if len(found) >= 3:
        data['fname3'].append(found[2]['practice'])
        data['street3'].append(found[2]['address1'])
        data['city3'].append(found[2]['city'])
        data['state3'].append(found[2]['state'])
        data['zip3'].append(found[2]['zip'])
        data['phone3'].append(found[2]['phone'])
    else:
        data['fname3'].append(None)
        data['street3'].append(None)
        data['city3'].append(None)
        data['state3'].append(None)
        data['zip3'].append(None)
        data['phone3'].append(None)

    # print(data)

DataFrame.from_dict(data, orient='columns').to_csv('./scriptResults/facilitiesList.csv', mode='w', na_rep='NULL',
                                                       index=False, header=False)

