#!/bin/bash
# for splitting the CSV list by equal amount for email blast
# navigate in the terminal to where the exported email list is
# change the filename to whichever the name of the list 
# change number to however many values you want per file
# will save as part 1, part 2 etc on wherever the old list is located

FILENAME=osteo-genius.csv
HDR=$(head -1 $FILENAME)
split -l 45000 $FILENAME xyz
n=1
for f in xyz*
do
     if [ $n -gt 1 ]; then 
          echo $HDR > Part${n}.csv
     fi
     cat $f >> Part${n}.csv
     rm $f
     ((n++))
done