// one off email sender with uuid

const fetch = require("node-fetch");
const csv = require("csv-parser");
const fs = require("fs");
const awsParamStore = require("aws-param-store");

require("dotenv").config();

let hologic;
let recipientObjects = [];

const prep = async () => {
  await awsParamStore
    .getParameter("/hologic/dev/sparkpost", { region: "us-east-1", accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY})
    .then((parameter) => {
      hologic = parameter.Value;
    });

    // put csv file name here
    fs
    .createReadStream("testEmails.csv")
    .pipe(csv())
    .on("data", (row) => {

      // let formatName = row.name ? row.name.charAt(0).toUpperCase() + row.name.slice(1).toLowerCase() : "" 
      let subjectName = row.name ? row.name.charAt(0).toUpperCase() + row.name.slice(1).toLowerCase().split(" ")[0]+"," : ""
      let name = row.name ? " "+ row.name.charAt(0).toUpperCase() + row.name.slice(1).toLowerCase().split(" ")[0]: ""
      recipientObjects.push({
        address: {
          email: row.email_address,
        },
        // modify substitution data as needed
        substitution_data: {
          email: row.email_address,
          unSubCampaign: row.campaign,
          name:name,
          subjectName: subjectName
        },
      });
    })
    .on("end", () => {
      console.log("CSV file successfully processed");
      sendEmail();
    });
};
const sendEmail = async () => {
  let response = await fetch(
    "https://api.sparkpost.com/api/v1/transmissions?num_rcpt_errors=3",
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        Authorization: hologic,
      },
      body: JSON.stringify({
        content: {
          // change template ID
          template_id: "osteoporosis-blast-genius-2",
        },
        // change campaign ID
        campaign_id: "osteoporosis-blast-2",
        recipients: recipientObjects,
      }),
    }
  );
  console.log(await response.json());
};

prep();
